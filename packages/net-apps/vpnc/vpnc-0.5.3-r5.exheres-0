# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part on 'vpnc-0.5.3.ebuild' from Gentoo Linux which is:
#    Copyright 1999-2008 Gentoo Foundation

SUMMARY="Client for Cisco VPN concentrator"
HOMEPAGE="http://www.unix-ag.uni-kl.de/~massar/vpnc/"
DOWNLOADS="http://www.unix-ag.uni-kl.de/~massar/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2 BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/libgcrypt[>=1.1.90]
        sys-apps/iproute2[>=2.6]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

src_prepare() {
    default

    edo sed -i \
        -e "s:^\(PREFIX=\).*:\1/usr/$(exhost --target):" \
        -e "s:^\(SBINDIR=\).*:\1\$\{BINDIR\}:" \
        -e "s:^\(MANDIR=\).*:\1/usr/share/man:" \
        -e "s:^\(DOCDIR=\).*:\1/usr/share/doc/${PNVR}:" \
        Makefile
    edo sed -i -e 's#/var/run/vpnc#/run/vpnc#g' vpnc-disconnect config.c vpnc-script.in
}

src_compile() {
    emake \
        CC="${CC}" \
        CFLAGS="${CFLAGS}" \
        LDFLAGS="${LDFLAGS} $(libgcrypt-config --libs) -lcrypto" \
        OPENSSL_GPL_VIOLATION=-DOPENSSL_GPL_VIOLATION
}

src_install() {
    default

    keepdir /etc/vpnc/scripts.d

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/vpnc 0755 root root
EOF
}

