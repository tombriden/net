PUSHDIVERT(-1)
#
#

_DEFIFNOT(`_DEF_DSPAM_MAILER_FLAGS', `lsDFMnqXz')
_DEFIFNOT(`DSPAM_MAILER_FLAGS', `A@/:|m')
ifdef(`DSPAM_MAILER_ARGS',, `define(`DSPAM_MAILER_ARGS', `FILE /run/dspam/dspam.sock')')
define(`_DSPAM_QGRP', `ifelse(defn(`DSPAM_MAILER_QGRP'),`',`', `Q=DSPAM_MAILER_QGRP,')')dnl

POPDIVERT

#########################################
###   DSpam Mailer specification   ###
#########################################

Mdspam, P=[IPC], F=_MODMF_(CONCAT(_DEF_DSPAM_MAILER_FLAGS,
DSPAM_MAILER_FLAGS), `DSPAM'),
                S=EnvFromSMTP/HdrFromL, R=EnvToL/HdrToL, E=\r\n,
                _OPTINS(`DSPAM_MAILER_MAXMSGS', `m=', `,
')_OPTINS(`DSPAM_MAILER_MAXRCPTS', `r=',
`,')_OPTINS(`DSPAM_MAILER_CHARSET', `C=', `, ')T=DNS/RFC822/SMTP,_DSPAM_QGRP
                A=DSPAM_MAILER_ARGS
