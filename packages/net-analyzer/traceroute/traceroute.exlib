# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ]

export_exlib_phases src_prepare src_install

SUMMARY="Utility for tracing IP packet routes"
DESCRIPTION="
New implementation of the traceroute utility for modern Linux systems. Backward
compatible with the traditional traceroute. Supports both IPv4 and IPv6, additional
types of trace (including TCP), allows some traces for unprivileged users.
"

BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        !net-misc/iputils[<20121221] [[
            description = [ traceroute6 is installed by iputils < 20121221 ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_COMPILE_PARAMS=( "env=yes" )
DEFAULT_SRC_INSTALL_PARAMS=( "prefix=/usr" )

traceroute_src_prepare() {
    default

    edo sed -i -e "s:\(exec_prefix =\).*:\1 /usr/$(exhost --target):" Make.rules
}

traceroute_src_install() {
    default

    dodir /usr/$(exhost --target)/bin
    dosym traceroute /usr/$(exhost --target)/bin/traceroute6
    dosym traceroute.8 /usr/share/man/man8/traceroute6.8
}

