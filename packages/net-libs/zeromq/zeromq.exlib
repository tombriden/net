# Copyright 2011 NAKAMURA Yoshitaka
# Copyright 2013 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require github [ user="${PN}" project="libzmq" release="v${PV}" suffix="tar.gz" ]

SUMMARY="The Intelligent Transport Layer"
HOMEPAGE+=" http://www.zeromq.org/"

LICENCES="LGPL-3"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build+run:
        dev-libs/libsodium
        dev-libs/libunwind
        sys-apps/util-linux
        doc? (
            app-doc/asciidoc
            app-text/xmlto
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-drafts  # required by pyzmq for testing
    --with-libsodium
    --without-pgm
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'doc documentation' )

# Tests share infrastructure and cannot be run in parallel (from INSTALL file).
DEFAULT_SRC_TEST_PARAMS=( -j1 )

src_test() {
    # Some of the tests are still flaky.
    local local_port_range="32768-60999"
    local whitelist=(
        "--connect LOOPBACK@${local_port_range}"
        "--connect LOOPBACK@1234"
        "--connect LOOPBACK@1235"
        "--connect LOOPBACK@5555"
        "--connect LOOPBACK@5556"
        "--connect LOOPBACK@5558"
        "--connect LOOPBACK@5561"
        "inet:0.0.0.0@0"
        "inet:0.0.0.0@5556"
        "unix-abstract:tmp-tester"
        "unix:${TEMP}/tmp*/socket"
        "unix:${WORK}/test_filter_ipc.sock"
        "unix:/tmp/test_pair_ipc"
        "unix:/tmp/test_rebind_ipc"
        "unix:/tmp/test_reconnect_ivl"
        "unix:/tmp/test_use_fd_ipc"
        "unix:/tmp/tester"
    )

    for addr in "${whitelist[@]}"; do
        esandbox allow_net $addr
    done
    default
    for addr in "${whitelist[@]}"; do
        esandbox disallow_net $addr
    done
}

