# Copyright 2009 Mike Kelly
# Copyright 2011 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Copyright 2012 NAKAMURA Yoshitaka <arumakanoy@gmail.com>
# Copyright 2012 Lasse Brun <bruners@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require launchpad
require setup-py [ import=setuptools blacklist="3" multibuild=false ]
require systemd-service
require option-renames [ renames=[ 'curses ncurses' ] ]

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="Wired and wireless network manager"
DESCRIPTION="
Wicd is an open source wired and wireless network manager for Linux
which aims to provide a simple interface to connect to networks with a
wide variety of settings.

Some of Wicd's features include:

   1. No Gnome dependencies (although it does require GTK), so it is
      easy to use in XFCE, Fluxbox, Openbox, Enlightenment, etc.
   2. Ability to connect to wired and wireless networks
   3. Profiles for each wireless network and wired network
   4. Many encryption schemes, some of which include WEP/WPA/WPA2 (and
      you can add your own)
   5. Remains compatible with wireless-tools
   6. Tray icon showing network activity and signal strength
   7. A full-featured console interface
"

BUGS_TO="alip@exherbo.org kimrhh@exherbo.org"
REMOTE_IDS="launchpad:${PN}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    gtk [[ description = [ Build the gtk backend ] ]]
    libnotify
    ncurses [[ description = [ Build a text mode client ] ]]
    pm-utils
"

DEPENDENCIES="
    build+run:
        dev-python/Babel[python_abis:*(-)?]
        dev-python/dbus-python[python_abis:*(-)?]
        gnome-bindings/pygobject:2[python_abis:*(-)?]
        gtk? ( gnome-bindings/pygtk:2[>=2.10][python_abis:*(-)?] )
        libnotify? ( dev-python/notify-python[python_abis:*(-)?] )
        ncurses? ( dev-python/urwid[>=0.9.8.3][python_abis:*(-)?] )
        pm-utils? ( sys-power/pm-utils[>=1.2.4] )
    run:
        net-wireless/wireless_tools
        net-wireless/wpa_supplicant
        sys-apps/net-tools
        sys-apps/util-linux [[ note = [ rfkill ] ]]
        virtual/dhcp-client
    suggestion:
        app-admin/ktsuss [[ description = [ make Wicd use ktsuss for graphical sudo ] ]]
        kde/kde-cli-tools [[ description = [ make Wicd use kdesu for graphical sudo ] ]]
        x11-libs/gksu [[ description = [ make Wicd use gksu for graphical sudo ] ]]
"

wicd_src_prepare() {
    setup-py_src_prepare

    edo rm po/*.po
}

wicd_src_configure() {
    args=(
        --no-install-gnome-shell-extensions
        --distro=gentoo
        --bin="/usr/$(exhost --target)/bin"
        --sbin="/usr/$(exhost --target)/bin"
        --lib="/usr/$(exhost --target)/lib"
        --docdir=/usr/share/doc/${PNVR}
        --python="/usr/$(exhost --target)/bin/python2"
        --systemd="${SYSTEMDSYSTEMUNITDIR}"
    )

    optionq gtk || args+=( --no-install-gtk )
    optionq libnotify || args+=( --no-use-notifications )
    optionq ncurses || args+=( --no-install-ncurses )
    optionq pm-utils || args+=( --no-install-pmutils ) && \
        args+=( --pmutils="/usr/$(exhost --target)/lib/pm-utils/sleep.d/55wicd" )

    LANG=en_US.UTF-8 edo python2 setup.py configure "${args[@]}"
}

wicd_src_compile() {
    LANG=en_US.UTF-8 setup-py_src_compile
}

wicd_src_install() {
    LANG=en_US.UTF-8 setup-py_src_install

    edo rm -Rf "${IMAGE}"/etc/init.d

    keepdir /etc/wicd/scripts/{pre,post}{,dis}connect
    keepdir /var/lib/wicd/configurations
    keepdir /var/log/wicd
}

